\contentsline {chapter}{LEMBAR PERNYATAAN}{i}{figure.caption.1}
\contentsline {chapter}{LEMBAR PENGESAHAN}{ii}{figure.caption.1}
\contentsline {chapter}{ABSTRAK}{iii}{figure.caption.1}
\contentsline {chapter}{\textit {ABSTRACT}}{iv}{figure.caption.1}
\contentsline {chapter}{LEMBAR PERSEMBAHAN}{v}{figure.caption.1}
\contentsline {chapter}{KATA PENGANTAR}{vi}{Item.3}
\contentsline {chapter}{DAFTAR ISI}{viii}{Item.7}
\contentsline {chapter}{DAFTAR GAMBAR}{x}{Item.7}
\contentsline {chapter}{DAFTAR TABEL}{xi}{Item.7}
\contentsline {chapter}{\numberline {I}\uppercase {Pendahuluan}}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Latar Belakang}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Rumusan Masalah}{1}{section.1.2}
\contentsline {section}{\numberline {1.3}Tujuan}{2}{section.1.3}
\contentsline {section}{\numberline {1.4}Batasan Masalah}{2}{section.1.4}
\contentsline {section}{\numberline {1.5}Metodologi Penyelesaian Masalah}{2}{section.1.5}
\contentsline {section}{\numberline {1.6}Sistematika Penulisan}{2}{section.1.6}
\contentsline {chapter}{\numberline {II}\uppercase {Tinjauan Pustaka}}{4}{chapter.2}
\contentsline {section}{\numberline {2.1}Reference Tracing}{4}{section.2.1}
\contentsline {section}{\numberline {2.2}Silabel}{4}{section.2.2}
\contentsline {section}{\numberline {2.3}Tanda Baca}{5}{section.2.3}
\contentsline {section}{\numberline {2.4}Himpunan Kalimat Minimum}{5}{section.2.4}
\contentsline {section}{\numberline {2.5}Algoritma \textit {Greedy}}{6}{section.2.5}
\contentsline {chapter}{\numberline {III}\uppercase {Rancangan Sistem}}{7}{chapter.3}
\contentsline {section}{\numberline {3.1}Sistem yang dikembangkan}{7}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Deskripsi Sistem}{7}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Data Penelitian}{8}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}Preprocessing}{8}{subsection.3.1.3}
\contentsline {subsection}{\numberline {3.1.4}Modified LTM Greedy Algorithm}{9}{subsection.3.1.4}
\contentsline {subsection}{\numberline {3.1.5}Semi LTM Greedy 1}{10}{subsection.3.1.5}
\contentsline {subsection}{\numberline {3.1.6}Semi LTM Greedy 2}{10}{subsection.3.1.6}
\contentsline {section}{\numberline {3.2}Metode Pengujian Sistem}{11}{section.3.2}
\contentsline {chapter}{\numberline {IV}\uppercase {Pengujian dan Analisis}}{12}{chapter.4}
\contentsline {section}{\numberline {4.1}Pengujian}{12}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Skenario 1 : Modified LTM Greedy}{12}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Skenario 2 : Semi LTM Greedy}{13}{subsection.4.1.2}
\contentsline {section}{\numberline {4.2}Analisis Hasil Pengujian}{13}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Monosilabis}{13}{subsection.4.2.1}
\contentsline {subsubsection}{\numberline {4.2.1.1}Data asli}{13}{subsubsection.4.2.1.1}
\contentsline {subsubsection}{\numberline {4.2.1.2}Skenario 1 : Modified LTM Greedy}{13}{subsubsection.4.2.1.2}
\contentsline {subsubsection}{\numberline {4.2.1.3}Skenario 2 : Semi LTM Greedy 1 dan Semi LTM Greedy 2}{13}{subsubsection.4.2.1.3}
\contentsline {subsection}{\numberline {4.2.2}Bisilabis}{15}{subsection.4.2.2}
\contentsline {subsubsection}{\numberline {4.2.2.1}Data asli}{15}{subsubsection.4.2.2.1}
\contentsline {subsubsection}{\numberline {4.2.2.2}Skenario 1 : Modified LTM Greedy}{15}{subsubsection.4.2.2.2}
\contentsline {subsubsection}{\numberline {4.2.2.3}Skenario 2 : Semi LTM Greedy 1 dan Semi LTM Greedy 2}{15}{subsubsection.4.2.2.3}
\contentsline {chapter}{\numberline {V}\uppercase {Kesimpulan dan Saran}}{18}{chapter.5}
\contentsline {section}{\numberline {5.1}Kesimpulan}{18}{section.5.1}
\contentsline {section}{\numberline {5.2}Saran}{18}{section.5.2}
\contentsline {chapter}{DAFTAR PUSTAKA}{19}{Item.46}
